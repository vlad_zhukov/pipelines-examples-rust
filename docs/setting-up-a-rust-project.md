# Setting up a Rust project

[Rust](https://www.rust-lang.org/) is a systems programming language that runs blazingly fast,
prevents segfaults, and guarantees thread safety.

[rustup](https://www.rustup.rs/) is an installer for Rust that helps manage multiple versions of
the compiler and toolchain. Install the rustup tool along with the stable version of the Rust
toolchain with the following command:

    curl https://sh.rustup.rs -sSf | sh -s -y
    source "${HOME}/.cargo/env"

Switch to the nightly version of the Rust compiler and toolchain with the following command:
    
    rustup default nightly


[Cargo](http://doc.crates.io/guide.html) is a tool that allows Rust projects to declare their
various dependencies and ensure that you’ll always get a repeatable build. It is part of the Rust
toolchain that is installed with rustup. This project was bootstrapped with the following command:

    cargo new pipelines-examples-rust
