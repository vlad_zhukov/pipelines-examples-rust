//! The example application reads input from stdin and writes to stdout.
//!
//! ```text
//! $ printf '111\na\nb' | cargo run --example alienlanguage
//! Case #1: 0
//!
//! $ printf '121\na\nb\n(ab)' | cargo run --example alienlanguage
//! Case #1: 2
//!
//! $ printf '222\naa\nab\n(ab)(ab)\n(ab)(ac)' | cargo run --example alienlanguage
//! Case #1: 2
//! Case #2: 1
//!
//! $ cargo run --example alienlanguage < input/sample.txt
//! Case #1: 2
//! Case #2: 1
//! Case #3: 3
//! Case #4: 0
//! ```

#![deny(warnings, missing_docs)]

use std::io;
use std::io::prelude::*;

extern crate alienlanguage;
use alienlanguage::Problem;

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let problem = Problem::from_lines(&mut lines);
    for case in problem {
        println!("{}", case);
    }
}
